import {createServer} from "http";
import Server from "./src/Server";

const PORT = process.env.PORT;

createServer(Server)
    .on("listening", () => console.log(`Server started at port ${PORT}`))
    .on("error", (error: NodeJS.ErrnoException) => console.log(error))
    .listen(PORT);