import {Document, model, Schema, Types} from "mongoose";

export interface ISession extends Document {
    _id: Types.ObjectId;
    uid: string;
    uuid: string;
    token: string;
    createdAt: number;
}

const sessionSchema = new Schema({
    uid: {
        type: Schema.Types.ObjectId,
        required: true
    },
    uuid: {
        type: Schema.Types.ObjectId,
        required: true
    },
    token: {
        type: String,
        required: true
    }
}, {versionKey: false, timestamps: true});

export const SessionModel = model<ISession>("sessions", sessionSchema);