import {Document, model, Schema, Types} from "mongoose";

export interface IClient extends Document {
    _id: Types.ObjectId;
    uid: Types.ObjectId;
    firstName: string;
    lastName: string;
}

const clientSchema = new Schema({
    uid: {
        type: Schema.Types.ObjectId,
        required: true
    },
    firstName: {
        type: String,
        lowercase: true,
        required: true
    },
    lastName: {
        type: String,
        lowercase: true,
        required: true
    }
}, {versionKey: false, timestamps: true});

export const ClientModel = model<IClient>("clients", clientSchema);