import {Document, model, Schema, Types} from "mongoose";

export interface IUser extends Document {
    _id: Types.ObjectId;
    email: string;
    password: string;
    role: string;
    isActive: boolean;
    createdAt: number;
}

const userSchema = new Schema({
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ["ADMIN", "CLIENT"],
        default: "CLIENT"
    },
    isActive: {
        type: Boolean,
        default: true
    }
}, {versionKey: false, timestamps: true});

export const UserModel = model<IUser>("users", userSchema);