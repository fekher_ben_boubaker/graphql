import {Document, model, Schema, Types} from "mongoose";
import {IUser} from "./user.model";

export interface IAdmin extends IUser, Document {
    _id: Types.ObjectId;
    uid: Types.ObjectId;
    username: string;
}

const adminSchema = new Schema({
    uid: {
        type: Schema.Types.ObjectId,
        required: true
    },
    username: {
        type: String,
        lowercase: true,
        required: true
    }
}, {versionKey: false, timestamps: true});

export const AdminModel = model<IUser>("admins", adminSchema);