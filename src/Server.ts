import {ApolloServer} from "apollo-server-express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as dotEnv from "dotenv";
import * as express from "express";
import * as mongoose from "mongoose";
import {Types} from "mongoose";

import schema from "./gql";
import {AdminModel, ClientModel} from "./models";
import {ISession, SessionModel} from "./models/session.model";
import {Token} from "./tools/token";

class Server {

    public app: express.Application;

    constructor() {
        dotEnv.config();
        this.app = express();
        this.connectToDB();
        this.config();
        this.setupGraphQL();
    }

    private connectToDB() {
        mongoose.connect(process.env.MONGOOSE_URI_CONNECTION, {dbName: process.env.DB_NAME, useCreateIndex: true, useNewUrlParser: true})
            .then(() => console.log("Connected to database"))
            .catch(() => console.log("Failed connect to database"));
    }

    private config() {
        this.app.use(bodyParser.json());
        this.app.use(cors());
    }

    private setupGraphQL() {
        const server = new ApolloServer({
            schema,
            context: async ({req: {headers: {authorization}}}) => {
                const token = authorization;
                const session: ISession = await SessionModel.findOne({token});
                if (!session) {
                    return null;
                }
                const payload: any = await Token.ensure(token);
                switch (payload.role) {
                    case "ADMIN":
                        return AdminModel.aggregate([
                            {$match: {_id: Types.ObjectId(session.uuid)}},
                            {$lookup: {from: "users", as: "profile", localField: "uid", foreignField: "_id"}},
                            {$unwind: "$profile"},
                        ]).then(admins => {
                            if (!admins.length) {
                                return;
                            }
                            const admin = admins[0];
                            const user = {...admin, role: "ADMIN"};
                            return {user};
                        });
                    case "CLIENT":
                        return ClientModel.aggregate([
                            {$match: {_id: Types.ObjectId(session.uuid)}},
                            {$lookup: {from: "users", as: "profile", localField: "uid", foreignField: "_id"}},
                            {$unwind: "$profile"},
                        ]).then(clients => {
                            if (!clients.length) {
                                return;
                            }
                            const client = clients[0];
                            const user = {...client, role: "CLIENT"};
                            return {user};
                        });
                    default:
                        return null;
                }
            }
        });
        server.applyMiddleware({app: this.app});
    }
}

export default new Server().app;