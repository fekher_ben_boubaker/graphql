import * as jwt from "jsonwebtoken";

const generate = payload => new Promise((resolve, reject) => {
    try {
        const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);
        resolve(token);
    } catch (e) {
        reject(e);
    }
});

const ensure = token => new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET_KEY, (err, payload) => {
        if (err) {
            reject(err);
        } else {
            resolve(payload);
        }
    });
});

export const Token = {
    ensure,
    generate
};