import PascalDirective from "./pascalcase";

export default {
    pascalcase: PascalDirective
};