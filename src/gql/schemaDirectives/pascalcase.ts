import {SchemaDirectiveVisitor} from "apollo-server-express";

export default class PascalDirective extends SchemaDirectiveVisitor {

    public visitFieldDefinition(field) {
        const {resolve} = field;
        field.resolve = async (...args) => {
            const result = await resolve.apply(this, args);
            if (typeof result === "string") {
                return `${result.substr(0, 1).toUpperCase()}${result.substr(1).toLowerCase()}`;
            }
            return result;
        };
    }
};