import Mutation from "./mutations";
import Query from "./queries";

import Types from "../nestedResolvers";

export default {
    Query,
    Mutation,
    ...Types
};
