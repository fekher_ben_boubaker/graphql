import {AuthenticationError} from "apollo-server-express";

import {UNAUTHORIZED} from "../../errors";

export default {
    me: (_, __, {user}) => {
        if (!user) {
             throw new AuthenticationError(UNAUTHORIZED);
        }
        return user;
    }
};