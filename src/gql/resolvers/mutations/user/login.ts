import {AuthenticationError, UserInputError} from "apollo-server-express";
import {SessionModel, UserModel} from "../../../../models";
import {Token} from "../../../../tools/token";
import {INVALID_DATA, INVALID_EMAIL, WRONG_PASSWORD} from "../../../errors";

export default async (_, {input: {email, password}}) => {
    if (!email || !password) {
        throw new UserInputError(INVALID_DATA);
    }
    const user = await UserModel.findOne({email});
    if (!user) {
        throw new AuthenticationError(INVALID_EMAIL);
    }
    console.log(user.role)
    return UserModel.aggregate([
        {$match: {email}},
        {
            $lookup: {
                from: user.role == "ADMIN" ? "admins" : "clients",
                as: "profile",
                localField: "_id",
                foreignField: "uid"
            }
        },
        {$unwind: "$profile"}
    ]).then(async (users: any) => {
        console.log(users);
        if (!users.length) {
            throw new AuthenticationError(INVALID_EMAIL);
        }
        const user = users [0];
        if (user.password != password) {
            throw new AuthenticationError(WRONG_PASSWORD);
        }
        const token = await Token.generate({uid: user._id, role: user.role});
        return new SessionModel({uid: user._id, uuid: user.profile._id, token}).save().then(() => token);
    });
}