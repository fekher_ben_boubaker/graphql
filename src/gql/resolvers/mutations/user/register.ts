import {ForbiddenError, UserInputError} from "apollo-server-express";
import {ClientModel, IClient, IUser, SessionModel, UserModel} from "../../../../models";
import {Token} from "../../../../tools/token";
import {EMAIL_EXISTS, INVALID_DATA} from "../../../errors";

export default (_, {input: {firstName, lastName, email, password}}) => UserModel.findOne({email}).then((user: IUser) => {
    if (!firstName || !lastName || !email || !password) {
        throw new UserInputError(INVALID_DATA);
    }
    if (user) {
        throw new ForbiddenError(EMAIL_EXISTS);
    }
    return new UserModel({email, password})
        .save()
        .then((nUser: IUser) => new ClientModel({uid: nUser._id, firstName, lastName})
            .save()
            .then(async (nClient: IClient) => {
                const token = await Token.generate({_id: nUser._id, role: "CLIENT"});
                return new SessionModel({uid: nUser._id, uuid: nClient._id, token}).save().then(() => token);
            })
        );
})