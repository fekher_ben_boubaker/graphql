import {makeExecutableSchema, SchemaDirectiveVisitor} from "apollo-server-express";

import directiveResolvers from "./directiveResolvers";
import resolvers from "./resolvers";
import schemaDirectives from "./schemaDirectives";
import typeDefs from "./typeDefs";

const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
    directiveResolvers,
    schemaDirectives
});

export default schema;