import Admin from "./admin";
import Client from "./client";
import Viewer from "./viewer";

export default {
    Viewer,
    Admin,
    Client
};