export default {
    __resolveType(obj, {user: {role}}, info) {
        switch (role) {
            case "ADMIN":
                return "Admin";
            case "CLIENT":
                return "Client";
        }
    }
};