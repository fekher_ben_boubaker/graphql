export default {
    id: _ => _._id.toString(),
    email: _ => _.profile.email,
    isActive: _ => _.profile.isActive,
    createdAt: _ => new Date(_.createdAt).getTime()
};