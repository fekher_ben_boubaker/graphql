import {AuthenticationError} from "apollo-server-express";

import {UNAUTHORIZED} from "../errors";

export default {
    admin: (next, source, args, {user}) => {
        if (user.role != "ADMIN") {
            throw new AuthenticationError(UNAUTHORIZED);
        }
        return next();
    }
};